import "tsconfig-paths/register";

import express from 'express';
import routes from '@routes';
import env from '@util/env';
import auth from '@auth';

const app = express();

app.use(express.json());
auth(app);
app.use('/', routes);

if (Boolean(env('IS_NODE') === 'true')) {
  const port = Number(env('EXPRESS_PORT')) || 3000;
  app.listen(port, () => {
    console.log(`Server listening at http://localhost:${port}...`);
  });
} else {
  const functions = require('firebase-functions');
  exports.campaigns = functions.region('europe-west3').https.onRequest(app);
}
