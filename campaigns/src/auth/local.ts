import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import user, { User } from '@db/user';
import { verifyPassword } from '@util/password-hash';

passport.use(new LocalStrategy(async (username, password, done) => {
  try {
    const currentUser = await user.getUser(username);
    if (currentUser && await verifyPassword(password, currentUser.password)) {
      console.log(`User "${username}" logged in successfully!`);
      done(null, currentUser);
    } else {
      console.warn(`Unable to login user "${username}"!`);
      done(null, false);
    }
  } catch (error) {
    done(error);
  }
}));

passport.serializeUser(async (user: User, done) => {
  if (user && user.username) {
    done(null, user.username);
  } else {
    done(new Error('Invalid user object!'));
  }
});

passport.deserializeUser(async (id: string, done) => {
  try {
    const currentUser = await user.getUser(id);
    if (currentUser) {
      done(null, currentUser);
    } else {
      const message = 'The session is broken!';
      console.error(message);
      done(new Error(message));
    }
  } catch (error) {
    done(error);
  }
});
