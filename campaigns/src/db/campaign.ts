import db from '@db/implementations';
import {
  IVerifiable,
  conditionalSet,
  conditionalMap,
  conditionalSetDate
} from '@util/sanitize';

export class CampaignPeriod implements IVerifiable {
  start: string;
  end?: string;
  repeat?: string;

  constructor(object: any) {
    conditionalSetDate([
      'start',
      'end',
      'repeat'
    ], object, this);
  }

  verify() {
    return !!this.start;
  }
}

export class Campaign implements IVerifiable {
  name: string;
  description?: string;
  priority: number;
  terminal_segments: string[];
  consumer_segments: string[];
  consumer_services: string[];
  periods: CampaignPeriod[];

  constructor(object: any) {
    conditionalSet([
      'name',
      'description',
      'priority',
      'terminal_segments',
      'consumer_segments',
      'consumer_services'
    ], object, this);
    conditionalMap('periods', object, this, value => new CampaignPeriod(value));
  }

  verify() {
    const retval = !!this.name
                && !!this.priority
                && !!this.terminal_segments
                && !!this.consumer_segments
                && !!this.consumer_services && this.consumer_services.length > 0
                && !!this.periods && this.periods.length > 0;
    
    if (!retval) {
      return false;
    }
    for (const period of this.periods) {
      if (!period.verify()) {
        return false;
      }
    }

    return true;
  }
}

async function getAllCampaigns(fields?: string | string[]): Promise<Campaign[]> {
  return await db.selectAll('Campaign', fields);
}

async function getCampaign(campaignName: string): Promise<Campaign> {
  return await db.select('Campaign', campaignName);
}

async function addCampaign(campaign: Campaign): Promise<void> {
  return await db.insert('Campaign', campaign, campaign.name);
}

async function updateCampaign(campaignName: string, campaign: Campaign): Promise<void> {
  return await db.update('Campaign', campaign, campaignName);
}

async function deleteCampaign(campaignName: string): Promise<boolean> {
  return await db.remove('Campaign', campaignName);
}

export default {
  getAllCampaigns,
  getCampaign,
  addCampaign,
  updateCampaign,
  deleteCampaign
};
