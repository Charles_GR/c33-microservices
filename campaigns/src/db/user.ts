import db from '@db/implementations';
import {
  IVerifiable,
  conditionalSet
} from '@util/sanitize';

export class User implements IVerifiable {
  username: string;
  password: string;

  constructor(object: any) {
    conditionalSet([
      'username',
      'password'
    ], object, this);
  }

  verify() {
    return !!this.username && !!this.password;
  }
}

async function getUser(username: string): Promise<User> {
  return await db.select('User', username);
}

async function addUser(user: User): Promise<void> {
  return await db.insert('User', user, user.username);
}

async function updateUser(username: string, user: User): Promise<void> {
  return await db.update('User', user, username);
}

async function deleteUser(username: string): Promise<boolean> {
  return await db.remove('User', username);
}

export default {
  getUser,
  addUser,
  updateUser,
  deleteUser
};
