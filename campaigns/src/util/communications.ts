import axios, {AxiosError, AxiosRequestConfig} from 'axios';
import env from '@util/env';

const isNode: boolean = Boolean(env('IS_NODE'));
const baseDomain: string = env('BASE_DOMAIN');
const domainSeparator: string = env('DOMAIN_SEPARATOR');
const timeout: number = Number(env('TIMEOUT'));

export interface InternalRequest {
  path: string;
  method: 'GET' | 'POST' | 'PUT' | 'DELETE';
  body?: any;
}

export interface InternalResponse {
  url: string;
  status: number;
  body?: any;
}

export default async function sendRequest(service: string, request: InternalRequest, cookie?: string): Promise<InternalResponse> {
  let url: string;

  if (isNode) {
    url = `https://${service}${domainSeparator}${baseDomain}/${request.path}`;
  } else {
    url = `https://${baseDomain}/${service}/${request.path}`;
  }

  try {
    const requestConfig: AxiosRequestConfig = {
      method: request.method,
      data: request.body,
      url,
      timeout
    };
    if (cookie) {
      requestConfig.headers = { Cookie: cookie };
    }

    const response = await axios.request(requestConfig);

    return {
      status: response.status,
      body: response.data,
      url
    };
  } catch (e) {
    const error: AxiosError = e;

    if (error.code === 'ECONNABORTED') {
      return null;
    } else if (error.response) {
      return {
        status: error.response.status,
        body: error.response.data,
        url
      };
    } else {
      console.log(error);
      throw new Error(error.message);
    }
  }
}
