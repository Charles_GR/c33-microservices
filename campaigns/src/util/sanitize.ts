import { Datastore } from '@google-cloud/datastore';

export interface IVerifiable {
  verify: () => boolean
}

export function conditionalSet<X, Y>(properties: string | string[], source: any, target: any, operation?: (value: X) => Y) {
  const process = (property: string) => {
    const value = source[property];

    if (value) {
      target[property] = operation ? operation(value) : value;
    }
  }

  if (Array.isArray(properties)) {
    properties.forEach(process);
  } else {
    process(properties);
  }
}

export function conditionalSetDate(properties: string | string[], source: any, target: any) {
  conditionalSet(properties, source, target, (value: string) => {
    const date = new Date(value);
    if (isNaN(date.valueOf())) {
      throw new Error('Invalid date!');
    }

    return date.toISOString();
  });
}

export function conditionalMap<T>(properties: string | string[], source: any, target: any, operation: (value: T) => T) {
  conditionalSet(properties, source, target, (value: any[]) => value.map(operation));
}

export function removeKey<T>(entity: any): T {
  entity && delete entity[Datastore.KEY];
  return entity;
}
