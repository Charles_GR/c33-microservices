import { Response } from 'express';

export default function (error: any, res: Response): void {
  console.log(error);
  res.status(500).send(error && { error: error.message });
}
