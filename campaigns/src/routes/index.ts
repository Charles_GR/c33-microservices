import { Router } from 'express';
import campaigns from '@routes/campaigns';

const routes = Router();

routes.use('/campaigns', campaigns);
routes.get('/', (req, res) => res.sendStatus(200));

export default routes;
