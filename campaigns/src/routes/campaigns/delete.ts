import { Request, Response } from 'express';
import campaign from '@db/campaign';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const result = await campaign.deleteCampaign(req.params.name);

    if (result) {
      res.sendStatus(200);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    dump(error, res);
  }
}
