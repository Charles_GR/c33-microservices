import { Request, Response } from 'express';
import campaign from '@db/campaign';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const entities = await campaign.getAllCampaigns(req.query.field as (string | string[]));
    res.status(200).send(entities);
  } catch (error) {
    dump(error, res);
  }
}
