import { Request, Response } from 'express';
import campaign, { Campaign } from '@db/campaign';
import dump from '@util/dump';
import sendRequest from '@util/communications';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    let entity: Campaign;
    try {
      entity = new Campaign(req.body);
    } catch (error) {
      console.log(error);
      res.sendStatus(400);
      return;
    }

    if (entity.verify()) {
      const consumerServices = entity.consumer_services;
      for (const cs of consumerServices) {
        const response = await sendRequest('consumer-services', {
          method: 'GET',
          path: `consumer-services/${cs}?field=name`
        }, req.headers.cookie);
    
        if (!response || response.status !== 200) {
          res.status(409)
             .send({ message: `Consumer service "${cs}" not found!` });
          return;
        }
      }

      await campaign.addCampaign(entity);
      res.sendStatus(201);
    } else {
      res.sendStatus(400);
    }
  } catch (error) {
    dump(error, res);
  }
}
