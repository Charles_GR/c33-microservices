import { Router } from 'express';
import get from './get';
import getAll from './get_all';
import add from './add';
import update from './update';
import _delete_ from './delete';

const campaignRoutes = Router();

campaignRoutes.get('/', getAll);
campaignRoutes.get('/:name', get);
campaignRoutes.post('/', add);
campaignRoutes.put('/:name', update);
campaignRoutes.delete('/:name', _delete_);

export default campaignRoutes;
