import { Request, Response } from 'express';
import campaign, { Campaign } from '@db/campaign';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const name: string = req.params.name;

    if (name) {
      let entity: Campaign;
      try {
        entity = new Campaign(req.body);
      } catch (error) {
        console.log(error);
        res.sendStatus(400);
        return;
      }

      await campaign.updateCampaign(name, entity);
      res.sendStatus(204);
    } else {
      res.sendStatus(400);
    }
  } catch (error) {
    dump(error, res);
  }
}
