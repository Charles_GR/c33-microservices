let getValue: (key: string) => string;

if (Boolean(process.env['IS_NODE'])) {
  getValue = key => process.env[key.toUpperCase()];
} else {
  const functions = require('firebase-functions');
  const config = functions.config();
  getValue = key => config.app[key.toLowerCase()];
}

export default getValue;
