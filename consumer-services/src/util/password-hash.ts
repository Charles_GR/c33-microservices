import bcrypt from "bcrypt";
import env from '@util/env';

const hashRounds = Number(env('HASH_ROUNDS')) || 10;

export async function hashPassword(password: string): Promise<string> {
  return bcrypt.hash(password, hashRounds);
}

export async function verifyPassword(password: string, hash: string): Promise<boolean> {
  return bcrypt.compare(password, hash);
}
