import { Datastore } from '@google-cloud/datastore';
import { DatastoreStore } from '@google-cloud/connect-datastore';
import { entity } from '@google-cloud/datastore/build/src/entity';
import session from 'express-session';
import { removeKey } from '@util/sanitize';
import db from './db';

const datastore = new Datastore();
const KEY: string = '__key__';

function encodeKey(type: string, key?: string): entity.Key {
  return datastore.key(key ? [type, key] : [type]);
}

function decodeKey(key: entity.Key): string {
  return key.name;
}

async function selectAll<T>(type: string, fields?: string | string[]): Promise<T[]> {
  let query = datastore.createQuery(type);
  if (fields) {
    query = query.select(fields);
  }

  const results = await datastore.runQuery(query);
  return results[0].map(value => removeKey(value));
}

async function select<T>(type: string, key: string): Promise<T> {
  const result = await datastore.get(encodeKey(type, key));
  return removeKey(result[0]);
}

async function insert<T>(type: string, object: T, key?: string): Promise<void> {
  await datastore.insert({
    key: encodeKey(type, key),
    data: object
  });
}

async function update<T>(type: string, object: T, key: string): Promise<void> {
  const encodedKey = encodeKey(type, key);

  const entity = (await datastore.get(encodedKey))[0];
  if (!entity) {
    throw new Error(`Entity "${key}" not found!`);
  }

  await datastore.update({
    key: encodedKey,
    data: Object.assign(entity, object)
  });
}

async function remove(type: string, key: string): Promise<boolean> {
  return (await datastore.delete(encodeKey(type, key)))[0].indexUpdates > 0;
}

function getSessionStore(session: any, expiration: number): session.Store {
  // TODO: Implement a periodic task to cleanup expired sessions.
  return new DatastoreStore({
    dataset: datastore,
    kind: 'Session',
    expirationMs: expiration
  });
}

const moduleExports: db = {
  selectAll,
  select,
  insert,
  update,
  remove,
  getSessionStore
};
export default moduleExports;
