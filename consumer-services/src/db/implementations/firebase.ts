import firebase from '@firebase/app';
import '@firebase/database';
import '@firebase/auth';
import session from 'express-session';
import db from './db';
import env from '@util/env';

const firebaseConfig = {
  databaseURL: 'https://fin-appops-connections-qa.firebaseio.com/',
  apiKey: 'AIzaSyAJMYvtf6tF0kWEkWrAQcDKY-NlVvzh5C4'
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
const database = firebaseApp.database();

const FIRESTORE_TIMEOUT: number = Number(env('FIRESTORE_TIMEOUT')) || 300;
const forceAuthenticate = () => authenticate(true);
setInterval(forceAuthenticate, FIRESTORE_TIMEOUT * 1000);
forceAuthenticate();

async function selectAll<T>(type: string, fields?: string | string[]): Promise<T[]> {
  await authenticate();

  const snapshot = await database.ref(type).once('value');

  const items: T[] = [];
  snapshot.forEach(function (child) {
    let item = child.val();

    if (fields) {
      if (!Array.isArray(fields)) {
        fields = [ fields ];
      }
      item = fields.reduce((obj: any, key: string) => { obj[key] = item[key]; return obj; }, {});
    }
    
    items.push(item);
  });

  return items;
}

async function select<T>(type: string, key: string): Promise<T> {
  await authenticate();

  const snapshot = await database.ref(type).child(key).once('value');
  return snapshot.val();
}

async function insert<T>(type: string, object: T, key?: string): Promise<void> {
  await authenticate();

  let realKey: string;

  if (key) {
    realKey = key;

    const snapshot = await database.ref(type).child(realKey).once('value');
    if (snapshot.exists()) {
      throw new Error('Already exists!');
    }
  } else {
    realKey = firebase.database().ref(type).push().key;
  }

  await database.ref(type).child(realKey).set(object);
}

async function update<T>(type: string, object: T, key: string): Promise<void> {
  await authenticate();

  return await database.ref(type).child(key).update(object);
}

async function remove(type: string, key: string): Promise<boolean> {
  await authenticate();

  const snapshot = await database.ref(type).child(key).once('value');
  if (!snapshot.exists()) {
    return false;
  }

  await database.ref(type).child(key).remove();
  return true;
}

async function authenticate(force?: boolean) {
  if (force || !firebaseApp.auth().currentUser) {
    await firebaseApp.auth().signInWithEmailAndPassword(env('DB_USERNAME'), env('DB_PASSWORD'));
  }
}

function getSessionStore(session: any, expiration: number): session.Store {
  // FIXME: The types for Firebase suck, so doing yet another unsafe import here. No expiration time set either.
  const FirebaseStore = require('connect-session-firebase')(session);
  return new FirebaseStore({ database });
}

const moduleExports: db = {
  selectAll,
  select,
  insert,
  update,
  remove,
  getSessionStore
};
export default moduleExports;
