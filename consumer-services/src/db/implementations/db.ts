import session from "express-session";

export default interface DB {
  selectAll: <T> (type: string, fields?: string | string[]) => Promise<T[]>,
  select: <T> (type: string, key: string) => Promise<T>,
  insert: <T> (type: string, object: T, key?: string) => Promise<void>,
  update: <T> (type: string, object: T, key: string) => Promise<void>,
  remove: (type: string, key: string) => Promise<boolean>,
  getSessionStore: (session: any, expiration: number) => session.Store
}
