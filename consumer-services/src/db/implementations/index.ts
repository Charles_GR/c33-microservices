import env from '@util/env';
import db from './db';

let database: db;

switch (env('DATABASE')) {
  case 'Datastore':
    database = require('./datastore').default;
    break;

  default: /* HERE BE DRAGONS: It defaults to Firebase if no database is selected. */
    database = require('./firebase').default;
    console.warn('Defaulting to Firebase...');
}

export default database;
