import { conditionalSet, IVerifiable } from '@util/sanitize';
import db from './implementations';

interface Parameter {
  key: string,
  value: string
}

export class ConsumerService implements IVerifiable {
  name: string;
  description?: string;
  access_name: string;
  is_developed: boolean;
  service_id: string;
  parameters?: Parameter[];

  constructor(object: any) {
    conditionalSet([
      'name',
      'description',
      'access_name',
      'is_developed',
      'service_id',
      'parameters'
    ], object, this);
  }

  verify() {
    return !!this.name
      && !!this.access_name
      && !!this.is_developed
      && !!this.service_id
      && (!this.parameters || this.parameters.every(parameter => !!parameter.key && !!parameter.value))
  }
}

async function getAllConsumerServices(fields?: string | string[]): Promise<ConsumerService[]> {
  return await db.selectAll('ConsumerService', fields);
}

async function getConsumerService(consumerServiceName: string): Promise<ConsumerService> {
  return await db.select('ConsumerService', consumerServiceName);
}

async function addConsumerService(consumerService: ConsumerService): Promise<void> {
  return await db.insert('ConsumerService', consumerService, consumerService.name);
}

async function updateConsumerService(consumerServiceName: string, consumerService: ConsumerService): Promise<void> {
  return await db.update('ConsumerService', consumerService, consumerServiceName);
}

async function deleteConsumerService(consumerServiceName: string): Promise<boolean> {
  return await db.remove('ConsumerService', consumerServiceName);
}

export default {
  getAllConsumerServices,
  getConsumerService,
  addConsumerService,
  updateConsumerService,
  deleteConsumerService
};
