import { conditionalSet, IVerifiable } from '@util/sanitize';
import db from './implementations';

export class ConsumerServiceImpl implements IVerifiable {
  name: string;
  description?: string;
  group: string;
  service: string;
  version: string;
  data: string;

  constructor(object: any) {
    conditionalSet([
      'name',
      'description',
      'group',
      'service',
      'version',
      'data'
    ], object, this);
  }

  verify() {
    return !!this.name
      && !!this.group
      && !!this.service
      && !!this.version
      && !!this.data;
  }
}

async function getAllConsumerServiceImpls(fields?: string | string[]): Promise<ConsumerServiceImpl[]> {
  return await db.selectAll('ConsumerServiceImpl', fields);
}

async function getConsumerServiceImpl(consumerServiceImplName: string): Promise<ConsumerServiceImpl> {
  return await db.select('ConsumerServiceImpl', consumerServiceImplName);
}

async function addConsumerServiceImpl(consumerServiceImpl: ConsumerServiceImpl): Promise<void> {
  return await db.insert('ConsumerServiceImpl', consumerServiceImpl, consumerServiceImpl.name);
}

async function updateConsumerServiceImpl(consumerServiceImplName: string, consumerServiceImpl: ConsumerServiceImpl): Promise<void> {
  return await db.update('ConsumerServiceImpl', consumerServiceImpl, consumerServiceImplName);
}

async function deleteConsumerServiceImpl(consumerServiceImplName: string): Promise<boolean> {
  return await db.remove('ConsumerServiceImpl', consumerServiceImplName);
}

export default {
  getAllConsumerServiceImpls,
  getConsumerServiceImpl,
  addConsumerServiceImpl,
  updateConsumerServiceImpl,
  deleteConsumerServiceImpl
};
