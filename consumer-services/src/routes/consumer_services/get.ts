import { Request, Response } from 'express';
import consumerService from '@db/consumer_service';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const entity = await consumerService.getConsumerService(req.params.name);

    if (entity) {
      res.status(200).send(entity);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    dump(error, res);
  }
}