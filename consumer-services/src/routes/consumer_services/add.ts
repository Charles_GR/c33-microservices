import { Request, Response } from 'express';
import consumerService, { ConsumerService } from '@db/consumer_service';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    let entity: ConsumerService;
    try {
      entity = new ConsumerService(req.body);
    } catch (error) {
      console.log(error);
      res.sendStatus(400);
      return;
    }

    if (entity.verify()) {
      await consumerService.addConsumerService(entity);
      res.sendStatus(201);
    } else {
      res.sendStatus(400);
    }
  } catch (error) {
    dump(error, res);
  }
}