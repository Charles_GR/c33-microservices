import { Request, Response } from 'express';
import consumerService, { ConsumerService } from '@db/consumer_service';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const name: string = req.params.name;

    if (name) {
      let entity: ConsumerService;
      try {
        entity = new ConsumerService(req.body);
      } catch (error) {
        console.log(error);
        res.sendStatus(400);
        return;
      }

      await consumerService.updateConsumerService(name, entity);
      res.sendStatus(204);
    } else {
      res.sendStatus(400);
    }
  } catch (error) {
    dump(error, res);
  }
}