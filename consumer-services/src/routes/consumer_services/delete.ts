import { Request, Response } from 'express';
import consumerService from '@db/consumer_service';
import dump from '@util/dump';
import sendRequest from '@util/communications';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const response = await sendRequest('campaigns', {
      method: 'GET',
      path: `campaigns`,
    }, req.headers.cookie);

    const inUse = response.body.some((campaign: any) => 
      Array.isArray(campaign.consumer_services)
        && campaign.consumer_services.includes(req.params.name)
    );
    if (inUse) {
      res.status(409)
         .send({ message: "Consumer service is in use by campaigns!" });
      return;
    }

    const result = await consumerService.deleteConsumerService(req.params.name);

    if (result) {
      res.sendStatus(200);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    dump(error, res);
  }
}