import { Router } from 'express';

import get from './get';
import getAll from './get_all';
import getData from './get_data';
import add from './add';
import update from './update';
import _delete_ from './delete';

const csImplRoutes = Router();

csImplRoutes.get('/', getAll);
csImplRoutes.get('/:name', get);
csImplRoutes.get('/:name/data', getData);
csImplRoutes.post('/', add);
csImplRoutes.put('/:name', update);
csImplRoutes.delete('/:name', _delete_);

export default csImplRoutes;
