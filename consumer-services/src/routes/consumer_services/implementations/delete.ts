import { Request, Response } from 'express';
import consumerServiceImpl from '@db/consumer_service_impl';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const result = await consumerServiceImpl.deleteConsumerServiceImpl(req.params.name);

    if (result) {
      res.sendStatus(200);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    dump(error, res);
  }
}