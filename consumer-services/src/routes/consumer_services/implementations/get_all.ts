import { Request, Response } from 'express';
import consumerServiceImpl from '@db/consumer_service_impl';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const entities = await consumerServiceImpl.getAllConsumerServiceImpls(req.query.field as (string | string[]));
    res.status(200).send(entities);
  } catch (error) {
    dump(error, res);
  }
}