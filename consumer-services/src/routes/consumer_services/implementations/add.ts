import { Request, Response } from 'express';
import consumerServiceImpl, { ConsumerServiceImpl } from '@db/consumer_service_impl';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    let entity: ConsumerServiceImpl;
    try {
      entity = new ConsumerServiceImpl(req.body);
    } catch (error) {
      console.log(error);
      res.sendStatus(400);
      return;
    }

    if (entity.verify()) {
      await consumerServiceImpl.addConsumerServiceImpl(entity);
      res.sendStatus(201);
    } else {
      res.sendStatus(400);
    }
  } catch (error) {
    dump(error, res);
  }
}