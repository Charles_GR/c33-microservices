import { Request, Response } from 'express';
import consumerServiceImpl from '@db/consumer_service_impl';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const entity = await consumerServiceImpl.getConsumerServiceImpl(req.params.name);

    if (entity) {
      res.status(200).send(entity.data);
    } else {
      res.sendStatus(404);
    }
  } catch (error) {
    dump(error, res);
  }
}