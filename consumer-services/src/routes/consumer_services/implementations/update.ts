import { Request, Response } from 'express';
import consumerServiceImpl, { ConsumerServiceImpl } from '@db/consumer_service_impl';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const name: string = req.params.name;

    if (name) {
      let entity: ConsumerServiceImpl;
      try {
        entity = new ConsumerServiceImpl(req.body);
      } catch (error) {
        console.log(error);
        res.sendStatus(400);
        return;
      }

      await consumerServiceImpl.updateConsumerServiceImpl(name, entity);
      res.sendStatus(204);
    } else {
      res.sendStatus(400);
    }
  } catch (error) {
    dump(error, res);
  }
}