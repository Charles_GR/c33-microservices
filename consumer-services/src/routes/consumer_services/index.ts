import { Router } from 'express';
import get from './get';
import getAll from './get_all';
import add from './add';
import update from './update';
import _delete_ from './delete';
import implementations from './implementations';

const csRoutes = Router();

csRoutes.use('/implementations', implementations);
csRoutes.get('/', getAll);
csRoutes.get('/:name', get);
csRoutes.post('/', add);
csRoutes.put('/:name', update);
csRoutes.delete('/:name', _delete_);

export default csRoutes;
