import { Request, Response } from 'express';
import consumerService from '@db/consumer_service';
import dump from '@util/dump';

export default async function (req: Request, res: Response): Promise<void> {
  try {
    const entities = await consumerService.getAllConsumerServices(req.query.field as (string | string[]));
    res.status(200).send(entities);
  } catch (error) {
    dump(error, res);
  }
}