import { Router } from 'express';
import consumer_services from '@routes/consumer_services';

const routes = Router();

routes.use('/consumer-services', consumer_services);
routes.get('/', (req, res) => res.sendStatus(200));

export default routes;
