import { Express, Request, Response, NextFunction } from 'express';
import passport from 'passport';
import session from 'express-session';
import env from '@util/env';
import { hashPassword } from '@util/password-hash';
import dump from '@util/dump';
import db from '@db/implementations';
import user, { User } from '@db/user';
import './local';

const secureCookie: boolean = Boolean(env('SECURE_COOKIE')) || false;
const sessionKey: string = env('SESSION_KEY');
const sessionExpiration: number = Number(env('SESSION_EXPIRATION')) || 300; /* Expiration time in seconds */

const sanitizePath = (path: string): string => path.replace(/\/$/, '');
const unauthenticatedEndpoints: string[] = [
  '/',
  '/login',
  '/register'
].map(sanitizePath);

export default function (app: Express): void {
  app.use(session({
    secret: sessionKey,
    name: 'session',
    cookie: { secure: secureCookie },
    rolling: true,
    unset: 'destroy',
    resave: false,
    saveUninitialized: false,
    store: db.getSessionStore(session, sessionExpiration * 1000)
  }));
  app.use(passport.initialize());
  app.use(passport.session());

  app.use((req: Request, res: Response, next: NextFunction) => {
    if (req.isAuthenticated() || unauthenticatedEndpoints.includes(sanitizePath(req.path))) {
      next();
    } else {
      res.sendStatus(401);
    }
  });

  app.post('/login', passport.authenticate('local'), (req: Request, res: Response) => {
    const currentUser: User = req.user as User;
    res.status(200).send(`Logged in as user "${currentUser.username}"!`);
  });

  app.post('/register', async (req: Request, res: Response) => {
    const currentUser: User = new User(req.body);
    if (!currentUser.verify()) {
      res.sendStatus(400);
      return;
    }

    try {
      currentUser.password = await hashPassword(currentUser.password);
      await user.addUser(currentUser);
      res.sendStatus(201);
    } catch (error) {
      dump(error, res);
    }
  });
}
