import { Request, Response, Router } from 'express';
import https from 'https';
import sendRequest from '@util/communications';
import env from '@util/env';

interface MicroserviceStatus {
  name: string,
  status: 'up' | 'down';
}

const services: string[] = env('MICROSERVICES').split(',');

async function getServiceStatus(service: string): Promise<MicroserviceStatus> {
  let status: boolean = false;

  try {
    const response = await sendRequest(service, {
      method: 'GET',
      path: ''
    });
  
    if (!response) {
      console.error(`Request timed out while contacting service "${service}"!`);
    } else if (response.status === 200) {
      status = true;
    } else {
      console.error(`Service "${service} responded with error code ${response.status}`)
    }
  } catch (error) {
    console.error(error);
  }
  
  return {
    name: service,
    status: status ? 'up' : 'down'
  };
}

async function getStatus(req: Request, res: Response): Promise<void> {
  const microservices: MicroserviceStatus[] = [];

  for (const service of services) {
    microservices.push(await getServiceStatus(service));
  }

  res.render('main', { layout: 'index', microservices });
}

const router = Router();
router.get('/', getStatus);

export default router;
