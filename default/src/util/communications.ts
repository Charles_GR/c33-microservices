import axios, { AxiosError } from 'axios';
import env from '@util/env';

const isNode: boolean = Boolean(env('IS_NODE'));
const baseDomain: string = env('BASE_DOMAIN');
const timeout: number = Number(env('TIMEOUT'));

export interface InternalRequest {
  path: string;
  method: 'GET' | 'POST' | 'PUT' | 'DELETE';
  body?: any;
};

export interface InternalResponse {
  url: string;
  status: number;
  body?: any;
}

export default async function sendRequest(service: string, request: InternalRequest): Promise<InternalResponse> {
  let url: string;
  
  if (isNode) {
    url = `https://${service}-dot-${baseDomain}/${request.path}`;
  } else {
    url = `https://${baseDomain}/${service}/${request.path}`;
  }

  try {
    const response = await axios.request({
      method: request.method,
      data: request.body,
      url,
      timeout
    });

    return {
      status: response.status,
      body: response.data,
      url
    };
  } catch (e) {
    const error: AxiosError = e;

    if (error.code === 'ECONNABORTED') {
      return null;
    } else if (error.response) {
      return {
        status: error.response.status,
        body: error.response.data,
        url
      };
    } else {
      console.log(error);
      throw new Error(error.message);
    }
  };
}
