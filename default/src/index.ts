import express from 'express';
import handlebars from 'express-handlebars';
import routes from '@routes';

const app = express();
const port = process.env.EXPRESS_PORT || 3000;

app.set('view engine', 'handlebars');
app.set('views', 'dist/views');
app.engine('handlebars', handlebars({
  layoutsDir: 'dist/views/layouts',
  defaultLayout: 'index'
}));

app.use(express.static('public'));
app.use('/', routes);

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}...`);
});
